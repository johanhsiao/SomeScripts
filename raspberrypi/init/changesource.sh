#!/bin/bash
mv /etc/apt/sources.list /etc/apt/sources.list.bak
touch /etc/apt/sources.list
echo "deb http://mirrors.bfsu.edu.cn/raspbian/raspbian/ buster main non-free contrib rpi" >> /etc/apt/sources.list
echo "deb-src http://mirrors.bfsu.edu.cn/raspbian/raspbian/ buster main non-free contrib rpi" >> /etc/apt/sources.list
mv /etc/apt/sources.list.d/raspi.list /etc/apt/sources.list.d/raspi.list.bak
touch /etc/apt/sources.list.d/raspi.list
echo "deb http://mirrors.bfsu.edu.cn/raspberrypi/ buster main ui" >> /etc/apt/sources.list.d/raspi.list
apt update
apt upgrade
apt install -y git vim
